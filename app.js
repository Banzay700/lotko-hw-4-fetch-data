import { cardTemplate, fetchFilmData, fetchCharactersData } from './utils.js'

const API_URL = 'https://ajax.test-danit.com/api/swapi/films'
const films = await fetchFilmData(API_URL)

const renderFilmInfo = films => {
   const cardsList = document.querySelector('.cards-wrapper')
   const loading = document.querySelector('.loading')

   if (!films) {
      loading.innerHTML += 'Oops!! Something went wrong...'
   } else {
      cardsList.innerHTML = ''
      films.forEach(film => (cardsList.innerHTML += cardTemplate(film)))
   }
}

renderFilmInfo(films)

const renderCharactersList = async films => {
   const charactersList = await fetchCharactersData(films)

   charactersList.forEach((characters, index) => {
      const charHtmlContainers = document.querySelectorAll('.char-list')
      const charactersList = characters.map(char => `${char.name}`).join(', ')

      charHtmlContainers[index].innerHTML = charactersList
   })
}

renderCharactersList(films)
