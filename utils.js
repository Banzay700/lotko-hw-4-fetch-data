export const fetchFilmData = async url => {
   try {
      const res = await fetch(url)
      const result = await res.json(res)

      return result
   } catch (error) {
      console.error(error.message)
   }
}

export const fetchCharactersData = async films => {
   const charactersData = films.map(film => Promise.all(film.characters.map(url => fetchFilmData(url))))
   const charactersList = await Promise.all(charactersData)

   return charactersList
}

export const cardTemplate = ({ name, id, openingCrawl }) => {
   return `
   <div class="card">
       <div class="film-name ">Star Wars: ${name} (Episode: ${id})</div>
       <div class="film-description">${openingCrawl}</div>
       <span class="characters">Characters:</span>
       <div class="char-list">
           <span></span><span></span><span></span><span></span><span></span>        
   </div>
</div>`
}
